BOARD:=SparkFun:avr:promicro
CHIP:=atmega32U4
SPEED:=16MHz

BOOTLOADER_IMG:=/home/whitelynx/.arduino15/packages/SparkFun/hardware/avr/1.1.12/bootloaders/caterina/Caterina-promicro16.hex


default: build/cooling-control.ino.hex


build:
	mkdir $@

build/cooling-control.ino.hex: cooling-control.ino build
	arduino --verify --board ${BOARD}:cpu=${SPEED}${CHIP} --pref build.path=build $<


upload: build/cooling-control.ino.hex /dev/ttyACM0
	stty -F /dev/ttyACM0 speed 1200
	stty -F /dev/ttyACM0 speed 57600
	avrdude -v -c avr109 -P /dev/ttyACM0 -p ${CHIP} -Uflash:w:$<:i

upload-usbtiny: build/cooling-control.ino.hex
	avrdude -v -c usbtiny -p ${CHIP} -Uflash:w:$<:i

upload-buspirate: build/cooling-control.ino.hex /dev/buspirate
	avrdude -v -c buspirate -P /dev/buspirate -p ${CHIP} -Uflash:w:$<:i


burn-bootloader-usbtiny: ${BOOTLOADER_IMG}
	avrdude -v -p atmega32u4 -c usbtiny -Uflash:w:$<:i -Ulock:w:0x2F:m

burn-bootloader-buspirate: ${BOOTLOADER_IMG} /dev/buspirate
	avrdude -v -p atmega32u4 -c buspirate -P /dev/buspirate -Uflash:w:$<:i -Ulock:w:0x2F:m


/dev/ttyACM0:
	inotifywait -e create /dev
	inotifywait -e attrib /dev

/dev/buspirate:
	inotifywait -e create /dev
	inotifywait -e attrib /dev


clean:
	rm -f build/cooling-control.ino.hex


.PHONY: default upload upload-usbtiny upload-buspirate burn-bootloader-usbtiny burn-bootloader-buspirate clean
