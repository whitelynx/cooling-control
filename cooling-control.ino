#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>


#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 32  // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET -1  // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


#define THERMISTOR_PIN 0  // A0

#define FLOW_SENSE_PIN 1  // TXO
#define FAN1_SENSE_PIN 0  // RXI
#define FAN2_SENSE_PIN 7
#define PUMP_SENSE_PIN 10

#define RPM_CALC_INTERVAL 200  // in milliseconds
#define FAN_DIVISOR 2          // 2 Hall effect sensor triggers per rotation
#define MS_PER_MINUTE 60000

int Vo;
float R2window[] = { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. };
unsigned char R2idx = 0;
float R2, logR2, Tc, Tf;
#define R1 10000
#define C1 1.009249522e-03
#define C2 2.378405444e-04
#define C3 2.019202697e-07

unsigned long lastRPMCalc = 0;
volatile unsigned char flowSenseCount = 0;
volatile unsigned char fan1SenseCount = 0;
volatile unsigned char fan2SenseCount = 0;
volatile unsigned char pumpSenseCount = 0;
unsigned int flowRPM = 0;
unsigned int fan1RPM = 0;
unsigned int fan2RPM = 0;
unsigned int pumpRPM = 0;

void onFlowSense() { flowSenseCount++; }
void onFan1Sense() { fan1SenseCount++; }
void onFan2Sense() { fan2SenseCount++; }
void onPumpSense() { pumpSenseCount++; }

void setup() {
	Serial.begin(9600);

	// SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
	if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
		Serial.println(F("SSD1306 allocation failed"));
		for(;;);  // Don't proceed, loop forever
	}

	// Show the Adafruit splash screen.
	display.display();

	// Set text display options
	display.setTextColor(WHITE);
	display.cp437(true);

	pinMode(FLOW_SENSE_PIN, INPUT_PULLUP);
	pinMode(FAN1_SENSE_PIN, INPUT_PULLUP);
	pinMode(FAN2_SENSE_PIN, INPUT_PULLUP);
	pinMode(PUMP_SENSE_PIN, INPUT_PULLUP);

	attachInterrupt(digitalPinToInterrupt(FLOW_SENSE_PIN), onFlowSense, RISING);
	attachInterrupt(digitalPinToInterrupt(FAN1_SENSE_PIN), onFan1Sense, RISING);
	attachInterrupt(digitalPinToInterrupt(FAN2_SENSE_PIN), onFan2Sense, RISING);
	attachInterrupt(digitalPinToInterrupt(PUMP_SENSE_PIN), onPumpSense, RISING);

	delay(500);

	lastRPMCalc = millis();
	sei();  // Enable interrupts
}

void loop() {
	Vo = analogRead(THERMISTOR_PIN);

	R2window[R2idx] = R1 * (1023.0 / (float)Vo - 1.0);
	R2idx = (R2idx + 1) % 10;

	// Calculate average R2 over the last 10 samples.
	R2 = 0;
	for (unsigned char idx = 0; idx < 10; idx++) {
		R2 += R2window[idx];
	}
	R2 /= 10;

	/*
	Serial.print("R2: ");
	Serial.print(Tc);
	Serial.println(" ohms");
	*/

	logR2 = log(R2);
	Tc = (1.0 / (C1 + C2 * logR2 + C3 * logR2 * logR2 * logR2)) - 273.15;
	Tf = (Tc * 9.0) / 5.0 + 32.0;

	unsigned long now = millis();
	int interval = now - lastRPMCalc;
	if(now >= (lastRPMCalc + RPM_CALC_INTERVAL)) {
		cli();  // Disable interrupts

		flowRPM = flowSenseCount * MS_PER_MINUTE / interval / FAN_DIVISOR;
		fan1RPM = fan1SenseCount * MS_PER_MINUTE / interval / FAN_DIVISOR;
		fan2RPM = fan2SenseCount * MS_PER_MINUTE / interval / FAN_DIVISOR;
		pumpRPM = pumpSenseCount * MS_PER_MINUTE / interval / FAN_DIVISOR;

		flowSenseCount = 0;
		fan1SenseCount = 0;
		fan2SenseCount = 0;
		pumpSenseCount = 0;

		lastRPMCalc = millis();
		sei();  // Enable interrupts

		Serial.print("Temperature: ");
		Serial.print(Tc);
		Serial.print(" C / ");
		Serial.print(Tf);
		Serial.println(" F");
		Serial.print("Flow: ");
		Serial.print(flowRPM);
		Serial.println(" RPM");
		Serial.print("Pump: ");
		Serial.print(pumpRPM);
		Serial.println(" RPM");
		Serial.print("Fan 1: ");
		Serial.print(fan1RPM);
		Serial.println(" RPM");
		Serial.print("Fan 2: ");
		Serial.print(fan2RPM);
		Serial.println(" RPM");
	}

	display.clearDisplay();
	display.setCursor(0, 0);
	display.print(F("T "));
	display.print(Tf);
	display.print(F("\370F "));
	display.print(Tc);
	display.println(F("\370C"));
	display.print(F("Flow: "));
	display.print(flowRPM);
	display.print(F("; Pump: "));
	display.println(pumpRPM);
	display.print(F("Fan1: "));
	display.print(fan1RPM);
	display.print(F("; Fan2: "));
	display.println(fan2RPM);
	display.display();

	delay(50);
}
